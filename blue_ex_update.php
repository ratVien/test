<?php
/**
 * Created by PhpStorm.
 * User: smykov
 * Date: 18.01.16
 * Time: 15:34
 */
error_reporting(E_ALL);
require_once(__DIR__ . "/../libs/db_pdo.php");
require_once(__DIR__ . "/../libs/define.php");
require_once(__DIR__ . "/../libs/functions.php");
require_once(__DIR__ . "/../libs/ApiBlueEx.php");

define("BLUE_EX_USERNAME", '2wtrade.llp');
define("BLUE_EX_PASSWORD", '123456');

$params = array();
echo "params:
\t--showQuery\t-\tsimple: \t--showQuery=y
\t--where\t\t-\tsimple: \t--where='status IN (6,8,9)'
\t--tracking\t-\tsimple: \t--tracking=6203033362
";
$bufArgv[] = array_shift($argv);
if(!empty($argv)){
    foreach ($argv as $item) {
        $param = trim(substr($item,0,strpos($item, '=')), '--');
        $value = substr($item,strpos($item, '=')+1);
        $params[$param] = $value;
        unset($param, $value, $item);
    }
}
$argv = $bufArgv;
unset($bufArgv);

$msg = date("Y-m-d H:i:s") . "; \r\n";

try {
    $cnt = 0;
    $cntError = 0;
    $cntNotFoundStatus = 0;
    $cntShippingNotFound = 0;
    $cntShippingEmptyTracking = 0;
    $shipping = array();

    $api = new ApiBlueEx(BLUE_EX_USERNAME, BLUE_EX_PASSWORD);
    $api->testMode = FALSE;

    if(!empty($params['tracking'])) {
        throw new Exception(print_r($api->getStatusOfOrder(array(trim($params['tracking']))), true));
    }

    $where = "
    (
    (`status` = 6 AND `status_id` NOT IN (19))
    OR
            (
                `status` = 9 AND `status_id` IN (16, 27)
            )
    )";
    if(!empty($params['where'])){
        $where = $params['where'];
    }
    $query = "
	SELECT *
    FROM `orders`
    WHERE {$where}
    /*AND `substatus` = 1*/
    ORDER BY id ASC
";
    $dataOrder = $db->query($query);

    if ($dataOrder->num_rows) {
        $cnt = 0;

        while ($order = $dataOrder->fetch_assoc()) {
//            echo "order_id: {$order['id']} | ";

            $query = "
			SELECT *
			FROM `shipping`
			WHERE `order_id` = {$order['id']} AND `delivery_id` = 1
		";

            $dataShipping = $db->query($query);

            if ($dataShipping->num_rows) {
                $shipping = $dataShipping->fetch_assoc();
                echo "\rcurrent: order_id: {$order['id']}|shipping: {$shipping['id']};\t error response: $cntError; success: $cnt; not found status: $cntNotFoundStatus;\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";

//                echo "shiping_id: {$shipping['id']} | ";
                if(empty($shipping['tracking'])){
//                    echo "ОШИБКА: нет трекера!\r\n";
                    $cntShippingEmptyTracking++;
                    continue;
                }

                $resultApi = $api->getStatusOfOrder(array($shipping['tracking']));

                if (isset($resultApi['status']) && $resultApi['status'] == 0) {
//                    echo "ОШИБКА при получении статуса заказа: {$resultApi['message']}\r\n" . (empty($resultApi['message']) ? print_r($resultApi, TRUE) : '');
                    $cntError++;
                    continue;
                }

                if (empty($resultApi['statusrow']) || !is_array($resultApi['statusrow']) || empty($resultApi['statusrow'][0]) || empty($resultApi['statusrow'][0]['statusmessage'])) {
//                    echo 'status not found | ';
                    $cntNotFoundStatus++;

                    echo $order['id'].' | '.$shipping['tracking']."\n";
                    continue;
                }
//                echo "Статус заказа успешно получен.\r\n";

                $status = $substatus = 6;
                $evento = false;
                $status_id = 12;
                $status_name = '';
                foreach (array_reverse($resultApi['statusrow']) as $statusObj) {
                    $evento = $statusObj['statusmessage'];
                    $dateStatus = '0000-00-00 00:00:00';
                    if (empty($evento)) {
                        continue;
                    }
                    if (!empty($statusObj['statusdate']) && !empty($statusObj['statustime'])) {
//                        $fecha = str_replace('/', '-', $statusObj['statusdate']);
                        $dateStatus = date("Y-m-d H:i:s", strtotime("{$statusObj['statusdate']} {$statusObj['statustime']}"));
                    }
                    if(stripos($evento, 'Delivered to') !== false){
                        $evento = 'Delivered';
                    }
/*
Code	Detail
OK	Delivered
IC	Incomplete Address
UA	Address Untraceable
RF	Refused to accept
RT	Return to shipper
CA	Customer not available
CO	Address Closed
NO	No Such Customer / Office
BC	Shipment at Hold on Customer`s Request
CW	Customer Not Answering Phone
AC	Area Closed
NI	CNIC Not Available
*/
                    switch ($evento) {
                        case 'Order information received, pending at Shipper\'s end.':
                            $shipping['date_accepted'] = $dateStatus;
                            $status_name = $evento;
                            $status = $substatus = '6';
                            $status_id = 12;
                            break;
                        case 'Delivered':
                            $shipping['date_approved'] = $dateStatus;
                            $status_name = $evento;
                            $status = $substatus = '8';
                            $status_id = 15;
                            break;
                        case 'Return to shipper':
                            $status_name = $evento;
                            $status = $substatus = '9';
                            $status_id = 17;
                            $shipping['date_returned'] = $dateStatus;
                            break;
                        case 'Refused to accept':// клиент отказал
                        case 'Incomplete Address'://Неполный адрес
                        case 'Customer not available'://клиент не доступен
                        case 'Address Closed'://Адрес Закрыт
                        case 'No Such Customer / Office'://нет такой человек в офисе
                        case 'Address Untraceable'://невозможно найти адрес
                        case 'Area Closed'://город закрыта  -- не возможно добраться до клиента
                        case 'CNIC Not Available'://"CNIC " не знаю что это - "No Available " не доступен
                            $status_id = 27;
                            $status_name = $evento;
                            $status = $substatus = '9';
                            $shipping['date_approved'] = $dateStatus;
                            break;
                    }
                }

                $query = "UPDATE `shipping`
                SET date_upd=NOW(), substatus='{$status}', date_approved = '{$shipping['date_approved']}', date_accepted = '{$shipping['date_accepted']}', date_returned = '{$shipping['date_returned']}'
                WHERE order_id = '{$order['id']}'";

                $db->query($query);
                if ($db->affected_rows) {
                    $cnt++;
                    $arrUpdated = array('status' => $status);
                    createLogOrderFull($db, $order['id'], $status_id, 0, empty($status_name)?$evento:$status_name, 'pay', $arrUpdated);
                    _log('blue_express_status', array('success', 'data' => array('status' => $status, 'order' => $order, 'shipping' => $shipping, 'query' => $query, 'resultApi' => json_encode($resultApi))));
                }
            }
            else {
                echo "\rcurrent: order_id: {$order['id']}|shipping: {$shipping['id']};\t error response: $cntError; success: $cnt; not found status: $cntNotFoundStatus;\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
//                echo 'shipping not found | ';
            }

//            echo "\r\n";
        }

    }
    else
        echo "Orders not found...\r\n";

} catch (Exception $e) {
    $msg .= " ERROR: {$e->getMessage()}\r\n";
}
$msg .= " shipping not found $cntShippingNotFound\r\n";
$msg .= " shipping empty tracking $cntShippingEmptyTracking\r\n";
$msg .= " Error response: $cntError\r\n";
$msg .= " Error obtaining the status of orders: $cntNotFoundStatus\r\n";
$msg .= " update orders $cnt\r\n";
echo $msg;
$sql = "INSERT INTO logs_update_orders VALUES (null, 1, NOW(), 'api', '{$db->real_escape_string($msg)}')";
$db->query($sql);
$db->close();