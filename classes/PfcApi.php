<?php
/**
 * Created by PhpStorm.
 * User: ratvien
 * Date: 13.04.16
 * Time: 13:42
 * Класс для работы с API курьерской службы http://www.pfcexpress.com/
 * @version 0.1
 *
 */

class PfcApi{
    const URL = "http://www.pfcexpress.com/webservice/APIWebService.asmx?WSDL";
    const KEY = '7afb52e2-7fda-4a39-96cb-b35972011af089117';
    const CUSTOMER_ID = '89117';

    private $client;

    function __construct()
    {
        $this->client = new SoapClient(self::URL);
    }

    function getCountry(){
        $params = ['secretkey' => self::KEY];
        if(!$success = json_decode($result = $this->client->getCountry($params)->getCountryResult)) {
            trigger_error("Error! " .MessageHandler::getMessage($result));
        }
        else return $success;
    }

    function getOrder_Track(){
        $params = ['Orderid' => 'R807521408070065',
                    'secretkey' => self::KEY];
        if(!json_decode($result = $this->client->getOrder_Track($params)->getOrder_TrackResult)) {
            //return $this->client->getCountry($params)->getCountryResult;

            if($result=='你的密钥不正确!'){
                return 'Your key is incorrect!';
            }
            else if($result=='服务器无法处理请求') 
                return 'The server was unable to process the request';
            else return 'Error!' . $result;

        }
        if($success = json_decode($result = $this->client->getOrder_Track($params)->getOrder_TrackResult)){
            return $success;
        }
        else trigger_error("Error! " .MessageHandler::getMessage($result));
        return false;
    }

    function InsertUpdateOrder(){
        $strorderinfo = "Style:2;GFF_CustomerID:" .self::CUSTOMER_ID.";GFF_ReceiveSendAddressID:;ConsigneeName:皇PFC;Country:US;
        Base_ChannelInfoID:HKRPOST;State:migdal haemek;City:migdal haemek;OrderStatus:3;Address1:hmelacha 3;
        Address2:null;CsRefNo:;Zipcode:23017;Contact:+972505742013;CusRemark:;";
        $strorderproduct = "MaterialRefNo:2020014210-A,MaterialQuantity:1,Price:5.5,Weight:0.1,EnName:null,WarehouseID:302,ProducingArea:null;";
        $stradd="";
        $params = ['strorderinfo' => $strorderinfo,            //Warehouse etc.
                    'strorderproduct' => $strorderproduct,                        //
                    'secretkey' => self::KEY];
        $result = $this->client->InsertUpdateOrder($params)->InsertUpdateOrderResult;
        if(strstr($result,'订单保存并提交成功!-')&&($orderStr = str_ireplace('订单保存并提交成功!-', '', substr($result, 0, -1)))){
            return $orderStr;
        }
        else trigger_error("Error! " .MessageHandler::getMessage($result));
            return false;
            
            //throw new \Exception("Error! " .MessageHandler::getMessage($result));


        //print_r(explode(';', $orderStr));
        //if($result)
    }

    function getChanel(){
        $params = ['secretkey' => self::KEY];
        $success = json_decode($result = $this->client->getChannel($params)->getChannelResult);
        return $result;
    }

    function getWarehouse(){
        $params = ['secretkey' => self::KEY];
        $success = json_decode($result = $this->client->getWarehouse($params)->getWarehouseResult);
        return $result;
    }

}