<?php
/**
 * Created by PhpStorm.
 * User: ratvien
 * Date: 14.04.16
 * Time: 18:59
 */

class MessageHandler{
    static function getMessage($message){
        if($success = json_decode($message)) return $success;
        else switch ($message){
            case '你的密钥不正确!':
                return 'The API key is incorrect!';
                break;
            case '你的密钥和客户ID不匹配':
                return 'Your key and customer ID does not match';
                break;
            case '服务器无法处理请求':
                return 'The server was unable to process the request';
                break;
            case '保存失败,请检查数据录入项是否有误!SKU的重量错误（为0或空）':
                return 'Save fails, check data entry items is wrong! SKU weight error (0 or empty)';
                break;
            default:
                return 'Error! $this->message';
        }
    }
}