<?php
/**
 * Created by PhpStorm.
 * User: ratvien
 * Date: 25.04.16
 * Time: 18:01
 */
/*
function __autoload($class_name){
    include $class_name.'.class.php';
}
*/
/*
class OnTime360Api
{
    const URL = "https://secure.ontime360.com/sites/BMS/ws/orders_internal.asmx?WSDL";
    const KEY = 'Mr.NitiponPonlawantheownerofBMSservice+66863927538';
    //const CUSTOMER_ID = '89117';

    private $client;

    function __construct()
    {
        $this->client = new SoapClient(self::URL);
    }
    
    

    /**
     * Создание заказа в API
     * @param $strOrderInfo
     * @param $strOrderProduct
     * @return array $arrTrackOrder [0 => track number,
     *                              1 => order number]
     */

/*
    public function СreateOrder()
    {

        $strOrderInfo = "Style:2;GFF_CustomerID:" .self::CUSTOMER_ID.";GFF_ReceiveSendAddressID:;ConsigneeName:皇PFC;Country:US;
        Base_ChannelInfoID:HKRPOST;State:migdal haemek;City:migdal haemek;OrderStatus:3;Address1:hmelacha 3;
        Address2:null;CsRefNo:;Zipcode:23017;Contact:+972505742013;CusRemark:null;TrackingNo:;";
        $strOrderProduct = "MaterialRefNo:2020014210-A,MaterialQuantity:1,Price:5.5,Weight:0.1,EnName:null,WarehouseID:302,ProducingArea:null;
        MaterialRefNo:2020014210-A,MaterialQuantity:1,Price:5.5,Weight:0.1,EnName:null,WarehouseID:302,ProducingArea:null;";
        $strAdd="";

//date('Y-m-d H:i:s', time())
        
        $ord = [
            'UTCSubmissionDate' => date('Y-m-d', time()),
            'AccountNumber' => "12345",
            'RequestedBy' => "Paul Hewson",
 //                      'DepartmentName' => '',
 //                      'SubaccountName' => '',
            'PriceSetName' => "Routine Medical",
            'UTCPickupDate' => date('Y-m-d', time()),
            'UTCDeliveryDate' => date('Y-m-d', time()),
            'Description' => "This is a test order submitted via the API.",
 //           'Comments' => '',
            'Weight' => 10,
            'Quantity' => 1,
            'Length' => 12,
            'Height' => 12,
            'Width' => 12,
            'Distance' => 100,
                       'ReferenceNumber' => '',
                       'PurchaseOrderNumber' => '',
            'DeclaredValue' => 100,
                       'TriggerWorkflowEvents' => '',
                       'IncomingTrackingNumber' => '',
                       'OutgoingTrackingNumber' => '',
            'CollectionName' => "Cedar Family Medicine",
            'CollectionContact' => "Nancy Placid",
            'CollectionStreet1' => "210 E Hersey St",
            'CollectionStreet2' => "",
            'CollectionCity' => "Ashland",
            'CollectionState' => "OR",
            'CollectionPostalCode' => "97520",
            'CollectionCountry' => "US",
                       'CollectionPhone' => "",
            'CollectionEmail' => "test@vesigo.com",
            'DeliveryName' => "DS Labs",
            'DeliveryContact' => "Jonathan Martinez",
            'DeliveryStreet1' => "1234 Rural Rd",
            'DeliveryStreet2' => "",
            'DeliveryCity' => "Talent",
            'DeliveryState' => "Oregon",
            'DeliveryPostalCode' => "97540",
            'DeliveryCountry' => "",
//            'DeliveryPhone' => "",
            'DeliveryEmail' => "info@vesigo.com",
            'Options' => "",
            'Items' => "",
        ];

        //$obj = (object)$ord;
        $params = [
            'SecurityKey' => self::KEY,
            'request' => $ord
        ];
        $result = $this->client->CreateOrder($params)->CreateOrderResult;
        return $result;
    }
}
*/

class OnTime360Api{

    const URL = "https://secure.ontime360.com/sites/BMS/ws/orders_internal.asmx?WSDL";
    const URL2 = "https://secure.ontime360.com/sites/BMS/ws/orders.asmx?WSDL";
    const ACCOUNT_ID = 'BMS';
    public $client;

    public function CreateOrder($order){
        $this->client = new SoapClient(self::URL);
        $option = ['trace' => true];
        $result = $this->client->CreateOrder($order,['trace' => 1])->CreateOrderResult;
        return $result;
    }

    public function GetStatus($track){
        $this->client = new SoapClient(self::URL2);
        $result = $this->client->GetStatus(self::ACCOUNT_ID, $track)->GetStatusResult;
        return $result;
    }
}


class OnTime360Order{
    public $SecurityKey;
    public $request;

    function __construct()
    {
        $this->SecurityKey = 'Mr.NitiponPonlawantheownerofBMSservice+66863927538';
    }
}

class request{
    public $UTCSubmissionDate;
    public $AccountNumber;
    public $RequestedBy;
    public $PriceSetName;
    public $UTCPickupDate;
    public $UTCDeliveryDate;
    public $Description;
    public $Weight;
    public $Quantity;
    public $Length;
    public $Height;
    public $Width;
    public $Distance;
    public $DeclaredValue;
    public $PurchaseOrderNumber;
    public $TriggerWorkflowEvents;
    public $CollectionName;
    public $CollectionContact;
    public $CollectionStreet1;
    public $CollectionStreet2;
    public $CollectionCity;
    public $CollectionState;
    public $CollectionPostalCode;
    public $CollectionCountry;
    public $CollectionPhone;
    public $CollectionEmail;
    public $DeliveryName;
    public $DeliveryContact;
    public $DeliveryStreet1;
    public $DeliveryStreet2;
    public $DeliveryCity;
    public $DeliveryState;
    public $DeliveryPostalCode;
    public $DeliveryCountry;
    public $DeliveryPhone;
    public $DeliveryEmail;
    public $Options;
    public $Items;
}

class Option{
    public $name;
    public $customValue;
}

class Items{
    public $Description;
    public $Comments;
    public $Height;
    public $Width;
    public $Depth;
    public $Weight;
}