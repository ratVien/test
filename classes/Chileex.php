<?php
/**
 * Created by PhpStorm.
 * User: ratvien
 * Date: 29.04.16
 * Time: 10:29
 */

class ChileEx{
    const URL = "http://qaws.ssichilexpress.cl/OSB/GenerarOTDigitalIndividual?wsdl";
    //const URL2 = "https://secure.ontime360.com/sites/BMS/ws/orders.asmx?WSDL";
    public $client;

    public function CreateOrder($order){
        $opts = array(
            'http'=>array(
                'user_agent' => 'PHPSoapClient'
            )
        );

        $context = stream_context_create($opts);
        $client = new SoapClient(self::URL, array('exceptions'=>true, 'trace' => true));

        //$this->client = new SoapClient('http://qaws.ssichilexpress.cl/OSB/GenerarOTDigitalIndividual?wsdl/', array('trace' => true));

        print_r($client);
        return false;
        $result = $this->client->GenerarOTDigitalIndividualRequest($order);
        return $result;
    }
}

class reqGenerarOTDigitalIndividual{
    public $codigoProducto;
    public $codigoServicio;
    public $comunaOrigen;
    public $numeroTCC;
    public $referenciaEnvio;
    public $montoCobrar;
    public $Destinatario;
    public $Direccion;
    public $Pieza;
}

class Destinatario{
    public $Nombre;
    public $cargoEmpresa;
    public $Email;
    public $Celular;
}

class Direccion{
    public $codigoRegion;
    public $Adicionales;
    public $Peso;
    public $Alto;
    public $Ancho;
    public $Largo;
}

class Pieza{
    public $Peso;
    public $Largo;
    public $Alto;
    public $ancho;
}

